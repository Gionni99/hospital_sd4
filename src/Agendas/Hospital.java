/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;


import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 *
 * @author a1906453
 */
public class Hospital {

    /**
     * @param args the command line arguments
     * @throws java.rmi.RemoteException
     * @throws java.rmi.AlreadyBoundException
     */
    public static void main(String[] args) throws RemoteException, NotBoundException, IOException{
        Registry registro = LocateRegistry.getRegistry(1099);
        Registry registro2 = LocateRegistry.getRegistry(1098);
        Hospital_Impl hospital = new Hospital_Impl((InterfaceAnest)registro.lookup("Anestesista"),(InterfaceCirurg)registro2.lookup("Cirurgiao"));
        Scanner scan = new Scanner(System.in);
        while(true){
            System.out.println("Que dia gostaria de agendar?\n Dia: ");
            Integer dia = scan.nextInt();
            System.out.println("Mês: ");
            Integer mes = scan.nextInt();
            System.out.println("Ano: ");
            Integer ano = scan.nextInt();
            System.out.println("Hora de inicio: ");
            Integer horaInicio = scan.nextInt();
            System.out.println("Hora de término: ");
            Integer horaFim = scan.nextInt();
            
            hospital.AbrirTrans(horaInicio, horaFim, dia, mes, ano);
            
        }
    }
    
}
