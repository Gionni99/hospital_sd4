/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;


import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.*;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author a1906453
 */
public class Hospital_Impl extends UnicastRemoteObject implements InterfaceHosp{

    private List<Horarios> horariosHosp;
    private Boolean Saleiro;
    
    private FileWriter arq;
    private PrintWriter gravarArq;
    private Integer ordem;
    private String destinoLog = "/home/todos/alunos/ct/a1906453/Área de Trabalho/";//Alterar aqui onde o Log é salvo
    
    private InterfaceAnest anestesisto;
    private InterfaceCirurg cirurgiano;
    private Random randomizador;
    
    private Horarios atualNaoEfetivado;
    
    public Hospital_Impl(InterfaceAnest anestesista, InterfaceCirurg cirurgiao) throws RemoteException, IOException{
        this.horariosHosp = new ArrayList<Horarios>();
        this.Saleiro = true;
        this.anestesisto=anestesista;
        this.cirurgiano=cirurgiao;
        this.arq = new FileWriter(destinoLog+"logHospINICIO.txt");
        this.gravarArq = new PrintWriter(arq);
        this.ordem = 0;
        this.randomizador = new Random();
    }

    
    @Override
    public void Efetivar_Transacao(Integer horaInicio, Integer horaFim, Integer dia, Integer mes, Integer ano) throws RemoteException {
        System.out.println("////Transação Efetivada:\nHoraInicio: "+atualNaoEfetivado.horarioInicio.toString()+
                "\nHoraFim: "+atualNaoEfetivado.horarioTermino.toString()+
                "\nData"+atualNaoEfetivado.dia.toString()+"/"+
                atualNaoEfetivado.mes.toString()+"/"+
                atualNaoEfetivado.ano.toString());
        gravarArq.printf("\n////Transação Efetivada:\nHoraInicio: "+atualNaoEfetivado.horarioInicio.toString()+
                "\nHoraFim: "+atualNaoEfetivado.horarioTermino.toString()+
                "\nData"+atualNaoEfetivado.dia.toString()+"/"+
                atualNaoEfetivado.mes.toString()+"/"+
                atualNaoEfetivado.ano.toString());
        try {
            arq.close();
        } catch (IOException ex) {
            Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
        }
        Horarios novoHorario = new Horarios(horaInicio,horaFim,dia,mes,ano);
        horariosHosp.add(novoHorario);
        Saleiro = true;
    }

    @Override
    public void Abortar(Transacao t) throws RemoteException {
        System.out.println("////Transação Abortada:\nHoraInicio: "+atualNaoEfetivado.horarioInicio.toString()+
                "\nHoraFim: "+atualNaoEfetivado.horarioTermino.toString()+
                "\nData"+atualNaoEfetivado.dia.toString()+"/"+
                atualNaoEfetivado.mes.toString()+"/"+
                atualNaoEfetivado.ano.toString());
        gravarArq.printf("\n////Transação Abortada:\nHoraInicio: "+atualNaoEfetivado.horarioInicio.toString()+
                "\nHoraFim: "+atualNaoEfetivado.horarioTermino.toString()+
                "\nData"+atualNaoEfetivado.dia.toString()+"/"+
                atualNaoEfetivado.mes.toString()+"/"+
                atualNaoEfetivado.ano.toString());
        try {
            arq.close();
        } catch (IOException ex) {
            Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
        }
        t.interrupt();
        Saleiro = true;
    }

    @Override
    public Boolean Obter_Decisao(Integer horaInicio,Integer horaFim,Integer dia,Integer mes,Integer ano) throws RemoteException {
        Boolean decisao = true;
        for(int i=0;i<horariosHosp.size();i++){
            Horarios hora=horariosHosp.get(i);
            
            if(hora.dia.equals(dia) && hora.mes.equals(mes) && hora.ano.equals(ano)){
                if((hora.horarioInicio<=horaFim && hora.horarioInicio>=horaInicio)||(hora.horarioTermino<=horaFim && hora.horarioTermino>=horaInicio)||
                        (hora.horarioInicio<=horaInicio && hora.horarioTermino>=horaFim)){
                    decisao = false;
                    gravarArq.printf("\nTransação falha;");
                }
            }
        }
        if(decisao==true){
            gravarArq.printf("\nTransação aceita;");
        }
        return decisao;
    }
//----------------------------------------------------------------------------------------------------------------------------------//
//----------------------------------------------Coordenador-------------------------------------------------------------------------//
//----------------------------------------------------------------------------------------------------------------------------------//
    @Override
    public String ObterEstado(Transacao t) throws RemoteException {
        return t.estado;
    }

    @Override
    public void AbrirTrans(Integer horaInicio, Integer horaFim, Integer dia, Integer mes, Integer ano) throws RemoteException {
        if(Saleiro==true){
            Saleiro=false;
            
            atualNaoEfetivado = new Horarios(horaInicio,horaFim,dia,mes,ano);
            
            try {
                arq = new FileWriter(destinoLog+"Log_Hosp"+ordem.toString()+".txt");
                ordem = ordem + 1;
                gravarArq = new PrintWriter(arq);
            } catch (IOException ex) {
                Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            gravarArq.printf("\n////Transação Iniciada:\nHoraInicio: "+atualNaoEfetivado.horarioInicio.toString()+
                "\nHoraFim: "+atualNaoEfetivado.horarioTermino.toString()+
                "\nData"+atualNaoEfetivado.dia.toString()+"/"+
                atualNaoEfetivado.mes.toString()+"/"+
                atualNaoEfetivado.ano.toString());
            
            try {
                Transacao t = new Transacao(horaInicio,horaFim,this,anestesisto,cirurgiano,dia,mes,ano);
            } catch (IOException ex) {
                Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            while(Saleiro==false){
                try {
                    this.wait(randomizador.nextInt());
                } catch (InterruptedException ex) {
                    Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            Saleiro = false;
            
            atualNaoEfetivado = new Horarios(horaInicio,horaFim,dia,mes,ano);
            
            try {
                arq = new FileWriter(destinoLog+"Log_Hosp"+ordem.toString()+".txt");
                ordem = ordem + 1;
                gravarArq = new PrintWriter(arq);
            } catch (IOException ex) {
                Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            gravarArq.printf("\n////Transação Iniciada:\nHoraInicio: "+atualNaoEfetivado.horarioInicio.toString()+
                "\nHoraFim: "+atualNaoEfetivado.horarioTermino.toString()+
                "\nData"+atualNaoEfetivado.dia.toString()+"/"+
                atualNaoEfetivado.mes.toString()+"/"+
                atualNaoEfetivado.ano.toString());
            
            try {
                Transacao t = new Transacao(horaInicio,horaFim,this,anestesisto,cirurgiano,dia,mes,ano);
            } catch (IOException ex) {
                Logger.getLogger(Hospital_Impl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
