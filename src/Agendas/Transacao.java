/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Agendas;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author Giovanni Bandeira
 */
//---------------------------------------------------------------------------------------------------------------------------------------------------//
class Transacao extends Thread{
    Integer horaInicio;
    Integer horaFim;
    Integer dia;
    Integer mes;
    Integer ano;
    
    InterfaceHosp hospital;
    InterfaceAnest anestesista;
    InterfaceCirurg cirurgiao;
    
    private String destinoLog = "/home/todos/alunos/ct/a1906453/Área de Trabalho/";//Alterar aqui onde o Log é salvo
    private FileWriter arq;
    private PrintWriter gravarArq;
    
    String estado;
    public Transacao(Integer horaInicio, Integer horaFim,InterfaceHosp hospital,InterfaceAnest anestesista, InterfaceCirurg cirurgiao, Integer dia, Integer mes, Integer ano) throws IOException {
        this.horaInicio=horaInicio;
        this.horaFim=horaFim;
        this.dia=dia;
        this.mes=mes;
        this.ano=ano;
        this.hospital=hospital;
        this.anestesista=anestesista;
        this.cirurgiao=cirurgiao;
        arq = new FileWriter(destinoLog+"Thread"+this.getId()+"_"+horaInicio.toString()
                +horaFim.toString()+"_"+dia.toString()+"_"+mes.toString()+"_"+ano.toString()+".txt");
        gravarArq = new PrintWriter(arq);
        this.start();
    }
    public void run(){
        try {
            estado="Iniciado";
            Boolean ok1,ok2,ok3;
            ok1 = hospital.Obter_Decisao(horaInicio, horaFim, dia, mes, ano);
            if(ok1==false){
                estado= estado +"\nHospital fail";
                gravarArq.printf(estado);
                arq.close();
                hospital.Abortar(this);
                this.interrupt();
            }
            estado=estado + "\nHospital ok";
            
            ok2 = anestesista.Obter_Decisao(horaInicio, horaFim, dia, mes, ano);
            if(ok2==false){
                estado=estado + "\nAnestesista fail";
                gravarArq.printf(estado);
                arq.close();
                anestesista.Abortar();
                hospital.Abortar(this);
                this.interrupt();
            }
            estado=estado + "\nAnestesista ok";
            
            ok3 = cirurgiao.Obter_Decisao(horaInicio, horaFim, dia, mes, ano);
            if(ok3==false){
                estado=estado + "\nCirurgiao fail";
                gravarArq.printf(estado);
                arq.close();
                cirurgiao.Abortar();
                anestesista.Abortar();
                hospital.Abortar(this);
                this.interrupt();
            }
            estado=estado + "\nCirurgiao ok";
            
            if(ok1==true && ok2==true && ok3==true){
                cirurgiao.Efetivar_Transacao(horaInicio, horaFim, dia, mes, ano);
                anestesista.Efetivar_Transacao(horaInicio, horaFim, dia, mes, ano);
                hospital.Efetivar_Transacao(horaInicio, horaFim, dia, mes, ano);
                estado = estado + "\nEfetivado";
                gravarArq.printf(estado);
                arq.close();
            }
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
}